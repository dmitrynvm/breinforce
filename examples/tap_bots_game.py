import gym
from breinforce import actors, common

common.utils.init()
env = gym.make('NoLimitHoldemSixPlayer-v0')

bots = [actors.TapBot()] * 6
env.add(bots)
obs = env.reset()

while True:
    action = env.move(obs)
    obs, rwd, done = env.step(action)
    if all(done):
        break

print(env.render())
