'''Classes and functions to create and manipulate cards and lists of
cards from a standard 52 card poker deck'''

from breinforce.common import const, errors


class Card(object):
    '''Cards are represented as 32-bit integers. Most of the bits are used
    and have a specific meaning, check the poker README for details:

                  bitrank  suit rank   prime
        +--------+--------+--------+--------+
        |xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
        +--------+--------+--------+--------+

        1) p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
        2) r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
        3) cdhs = suit of card (bit turned on based on suit of card)
        4) b = bit turned on depending on rank of card
        5) x = unused

    Parameters
    ----------
    string : str
        card string of format '{rank}{suit}' where rank is from
        [2-9, T/t, J/j, Q/q, K/k, A/a] and suit is from
        [S/s, H/h, D/d, C/c]

    Examples
    ------
    Card('TC'), Card('7H'), Card('ad')...
    '''

    def __init__(self, string: str) -> None:

        rank_char, suit_char = string.upper()
        rank_cond = rank_char in const.RANKS
        suit_cond = suit_char in const.SUITS
        if not rank_cond or not suit_cond:
            rank_char, suit_char = suit_char, rank_char

        rank_int = const.RANKS_TO_INTS[rank_char]
        suit_int = const.SUITS_TO_INTS[suit_char]
        rank_prime = const.PRIMES[rank_int]

        bitrank = 1 << rank_int << 16
        suit = suit_int << 12
        rank = rank_int << 8

        self.value = bitrank | suit | rank | rank_prime

    def __str__(self) -> str:
        suit_int = (self.value >> 12) & 0xF
        rank_int = (self.value >> 8) & 0xF
        suit = const.RAW_SUITS[suit_int]
        rank = const.STR_RANKS[rank_int]
        return f'{rank}{suit}'

    def __repr__(self) -> str:
        suit_int = (self.value >> 12) & 0xF
        rank_int = (self.value >> 8) & 0xF
        suit = const.RAW_SUITS[suit_int]
        rank = const.STR_RANKS[rank_int]
        return f'{rank}{suit}'

    def __and__(self, other):
        if isinstance(other, Card):
            other = other.value
        return self.value & other

    def __rand__(self, other):
        return other & self.value

    def __or__(self, other):
        if isinstance(other, Card):
            other = other.value
        return self.value | other

    def __ror__(self, other):
        return other | self.value

    def __lshift__(self):
        return self.value << other

    def __rshift__(self, other):
        return self.value >> other

    def __eq__(self, other):
        if isinstance(other, Card):
            return bool(self.value == other.value)
        raise NotImplementedError('only comparisons of two cards allowed')
