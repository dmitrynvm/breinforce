import gym
import uuid
import random
import string
import toml


def init():
    ''' Adds the local initialization envs to the global OpenAI Gym list.
    '''
    configs = toml.load('config.toml')['envs']
    entrypoint = 'breinforce.envs:PokerEnv'
    env_names = [env.id for env in gym.envs.registry.all()]
    for env_name, env_config in configs.items():
        if env_name not in env_names:
            gym.envs.registration.register(
                id=env_name,
                entry_point=entrypoint,
                kwargs={'props': env_config}
            )


def flatten(actions):
    """
    Flattens the actions dictionary

    Args:
        actions (dict): actions dictionary from the env

    Returns:
        dict: dictionary without nested values
    """
    out = {}
    for k, v in actions.items():
        if isinstance(v, dict):
            for ik, iv in v.items():
                out[f'{k}_{ik}'] = iv
        else:
            out[k] = v
    return out


def guid(size, mode='int'):
    """
    Generates unique object identifier

    Args:
        mode (str): mode of generating identifier

    Returns:
        str: generated identifier
    """
    out = ''
    if mode == 'int':
        out = str(uuid.uuid4().int)[:size]
    else:
        out = ''.join(random.choice(string.ascii_lowercase) for _ in range(size))
    return out
