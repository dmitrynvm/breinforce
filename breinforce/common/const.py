import os
from pathlib import Path

BASE_DIR = Path(__file__).parent.parent

ENVS_DIR = os.path.join(BASE_DIR, 'envs')

VIEW_DIR = os.path.join(BASE_DIR, 'templates')

RANKS = ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A']
SUITS = ['S', 'H', 'D', 'C']
STR_RANKS = '23456789TJQKA'
INT_RANKS = list(range(13))
PRIMES = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41]
RANKS_TO_INTS = dict(zip(list(STR_RANKS), INT_RANKS))
SUITS_TO_INTS = {
    'S': 1,  # spades
    'H': 2,  # hearts
    'D': 4,  # diamonds
    'C': 8,  # clubs
}

RAW_SUITS = {
    1: 's',  # spades
    2: 'h',  # hearts
    4: 'd',  # diamonds
    8: 'c',  # clubs
}

PRT_SUITS = {
    1: chr(9824),  # spades
    2: chr(9829),  # hearts
    4: chr(9830),  # diamonds
    8: chr(9827),  # clubs
}