import random
from breinforce.common.types import Action
from breinforce.common.utils import flatten
from . import BaseBot


class WildBot(BaseBot):

    def move(self, obs):
        actions = [Action(*item) for item in flatten(obs['actions']).items()]
        return random.choice(actions)
