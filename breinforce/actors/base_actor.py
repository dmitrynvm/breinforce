
class BaseActor(object):

    def __init__(self):
        pass

    def move(self, obs):
        raise NotImplementedError()
