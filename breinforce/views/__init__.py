from . import jsonify
from . import poker888
from . import pokerstars

views = {
    'jsonify': jsonify,
    'poker888': poker888,
    'pokerstars': pokerstars
}

__all__ = [
    'views'
]
