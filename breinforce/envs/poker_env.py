import gym
from pydux import create_store
from breinforce.views import views
from breinforce.common.types import Episode
from breinforce.game import engine


class PokerEnv(gym.Env):

    def __init__(self, props):
        self.store = create_store(
            engine.reducer,
            engine.init_state(props)
        )
        self.props = props
        self.actors = None
        self.history = []

    def move(self, obs):
        return self.players[self.state.player].move(obs)

    def step(self, action):
        player = self.state.player
        self.store.dispatch(engine.step_msg(action))
        self.history += [Episode(self.state.deepcopy(), player, action)]
        return engine.results(self.state)

    def add(self, players):
        self.players = players

    def render(self, mode='pokerstars'):
        return views[mode].render(self.history)

    def reset(self):
        self.store.dispatch(engine.reset_msg())
        self.history = []
        return engine.get_observation(self.state)

    @property
    def state(self):
        return self.store.get_state()
